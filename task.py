import sys, getopt, pandas, os, shutil

listInput = sys.argv[1:]

def check_is_exist() -> pandas:
    try:
        return pandas.read_csv(sys.argv[1], on_bad_lines='skip')
        
    except IndexError:
        raise Exception('Please provide a valid path file!')

def check_index(type: str) -> str:
    try:
        if type == 'spec_file':
            if "-t" in listInput: 
                if (sys.argv[3] == 'json') or (sys.argv[3] == 'text'):
                    return sys.argv[3]
                else:
                    raise Exception('Please provide a valid file output, eg. "json" or "text"')
            else:
                return 'null'

        elif type == 'from_file':
            os.path.isfile(sys.argv[1])
            return sys.argv[1]
        
    except IndexError:
        raise Exception('Please provide a valid file output, eg. "json" or "text"')

def get_absolute_path(path: str) -> str:
    return os.path.abspath(path+"/..")

def get_filename(index: int) -> str:
    file_full = os.path.basename(sys.argv[index])
    return os.path.splitext(file_full)[0]

def print_default(read_file: pandas, base_path: str, file_name: str):
    read_file.to_json(base_path+"/"+file_name+".text")

    print('Process has been finished')

def start():
    index = 0

    try:
        for currentArg in listInput:
            countLen = len(listInput)
            index += 1

            if currentArg in ("-h", "--help"):
                print ("Welcome to converter file by phyton\n")
                print ("In here we can specific change a format file with command like: ")
                print ("1. flag '-t' : for change file format to selected format eg. json or text")
                print ("2. flag '-o' : for moving file from origin path to destination file path\n")
                print ("Example command :")
                print ("1. python3 task.py log/text.log -t json")
                print ("2. python3 task.py log/text.log -t json -o log/text.json")

            elif currentArg == "-m":
                print ("Displaying file path: ", sys.argv[0])

            elif currentArg == "-o":
                type_file = check_index('spec_file')
                from_file = check_index('from_file')

                # target file
                base_target_path = get_absolute_path(sys.argv[index+1])
                target_file_name = get_filename(index+1)

                if type_file == 'json':
                    shutil.copyfile(from_file, base_target_path+"/"+target_file_name+".json")
                    os.remove(sys.argv[1])
                    
                elif type_file == 'text':
                    shutil.copyfile(from_file, base_target_path+"/"+target_file_name+".text")
                    os.remove(sys.argv[1])
                
                elif "-t" not in listInput:
                    shutil.copyfile(from_file, sys.argv[index+1])
                    os.remove(sys.argv[1])


                print('Process has been finished')

            elif currentArg == "-t":
                type_file = check_index('spec_file')
                base_path = get_absolute_path(sys.argv[1])

                read_file = check_is_exist()
                file_name = get_filename(1)

                if "-o" not in listInput:
                    if type_file == 'json':
                        read_file.to_json(base_path+"/"+file_name+".json")
                        os.remove(sys.argv[index-1])
                    
                    elif type_file == 'text':
                        read_file.to_csv(base_path+"/"+file_name+".text", header=None, index=None)
                        os.remove(sys.argv[index-1])

                    print('Process has been finished')

            elif countLen == 1 :
                read_file = check_is_exist()
                base_path = get_absolute_path(sys.argv[1])
                file_name = get_filename(1)

                print_default(read_file, base_path, file_name)
                os.remove(sys.argv[1])


    except getopt.error as error:
        print(str(error))

if __name__ == "__main__":
    start()