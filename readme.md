## Simple CLI Application

A simple CLI command build by Python. This file function for managing file format

### How run the apps :

#### 1. Clone the apps
```
$ git clone https://gitlab.com/AdiNugroho13/python-task-cli.git
```

#### 2. Install dependencies
Make sure that we have already installed python and some dependencies on the code.
For the download python links, we can go by [here.](https://www.python.org/downloads/)

#### 3. Launch the apps
Go to project file directory, and we can type command bellow for how to specific running the apps
```
$ python3 task.py -h
```

## Thank you very much!